const router = require("express").Router();
const mongoose = require("mongoose");
const htmlDocx = require("html-docx-js");
const d_download = require("downloadjs");
var fs = require("fs");
var pdf = require("html-pdf");
const path = require("path");
const Story = require("../../../models/stories");
const key = require("../../../config/keys");

router.get("*", (req, res, next) => {
  if (req.user) {
    next();
  } else {
    res.render("errors/401");
  }
});

router.post("/hide", async (req, res) => {
  let msg = "";
  try {
    let story = await Story.findOne({ s_ID: req.body.id });
    if (story) {
      if (req.body.p === "true") {
        story.published = false;
        msg = "Story is now in secret... ㊙️✌️";
      } else {
        story.published = true;
        msg = "Story Is Now Published... 💯";
      }
      let ex = await story.save();
      res.json({ status: 200, msg: msg });
    } else {
      res.json({ status: 404, msg: "Story not Found... 🤡" });
    }
  } catch (e) {
    res.json({ status: 500, msg: e });
  }
});

router.post("/delete", async (req, res) => {
  try {
    let story = await Story.findOneAndDelete({ s_ID: req.body.id });
    if (story) {
      res.json({ status: 200, msg: "Story Deleted... ☠️☠️☠️" });
    } else {
      res.json({ status: 404, msg: "Story not Found... 🤡" });
    }
  } catch (e) {
    res.json({ status: 500, msg: e });
  }
});

router.post("/addLike", async (req, res) => {
  try {
    let id = req.body.id;
    let user = req.user.userName;
    let s = await Story.findOne({ s_ID: id });
    if (s) {
      s.likes += 1;
      s.liked_by.push(user);
      await s.save();
      res.json({ status: 200 });
    } else {
      res.json({ status: 700 });
    }
  } catch (e) {
    console.error("error: " + e);
    res.json({ status: 500 });
  }
});

router.post("/removeLike", async (req, res) => {
  try {
    let id = req.body.id;
    let user = req.user.userName;
    let s = await Story.findOne({ s_ID: id });

    if (s) {
      for (var i = 0; i < s.liked_by.length; i++) {
        if (s.liked_by[i] === user) {
          s.liked_by.splice(i, 1);
        }
      }
      s.likes -= 1;
      await s.save();
      res.json({ status: 200 });
    } else {
      res.json({ status: 700 });
    }
  } catch (e) {
    console.error("error: " + e);
    res.json({ status: 500 });
  }
});

router.get("/download/:id", async (req, res) => {
  let sid = req.params.id;
  try {
    let story = await Story.findOne({ s_ID: sid.toString() });
    var finalHtml = '<html><head><meta charset="UTF-8"></head><body>';
    finalHtml += story.s_body.html;
    finalHtml += "</body></html>";
    // var converted = htmlDocx.asBlob(finalHtml);
    let dir = "./temp";
    let xtra = Date.now() + key.super_key.yagami + ".pdf";

    let linkFile = dir + "/" + xtra;
    let file = await createPdf(finalHtml, linkFile);

    res.download(file, story.title + ".pdf", err => {
      if (err) console.log(err);
      else {
        setTimeout(() => {
          fs.unlinkSync(file);
        }, 3000);
      }
    });
  } catch (e) {
    console.log(e);
  }
});

router.post("/getStory", async (req, res) => {
  let story = await Story.find({
    published: true,
    title: new RegExp(req.body.data, "i")
  });
  let data = [];
  let obj;
  if (story) {
    story.forEach(d => {
      obj = {
        title: d.title,
        link: d.link
      };
      data.push(obj);
    });
    res.json({ status: 2000, data: data });
  } else {
    res.json({ status: 4000 });
  }
});

let createPdf = (finalHtml, linkFile) => {
  return new Promise((resolve, reject) => {
    var options = { format: "A4", width: "6in" };
    pdf.create(finalHtml, options).toFile(linkFile, (err, res) => {
      if (err) {
        console.log(err);
        reject(Error(err));
      }
      console.log(res); // { filename: '/app/businesscard.pdf' }
      let file = fs.readFileSync(linkFile);
      console.log(file);
      resolve(linkFile);
    });
  });
};
module.exports = router;

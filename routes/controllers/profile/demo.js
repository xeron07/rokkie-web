const router = require("express").Router();
const mongoose = require("mongoose");
const Story = require("../../../models/stories");
const User = require("../../../models/users");

router.get("/:st_link", async (req, res) => {
  let story = await Story.findOne({ link: req.params.st_link });
  if (story) {
    if (req.user && req.user.userName === story.publisher_id) {
      let writer = await User.findOne({ userName: story.publisher_id });
      console.log(writer);
      let obj = {
        story,
        writer
      };
      res.render("read/index", obj);
    }
  }

  res.render("errors/401");
});

module.exports = router;

const router = require("express").Router();
const mongoose = require("mongoose");
const User = require("../../../models/users");

// router.get("*", (req, res, next) => {
//   if (req.user) {
//     next();
//   } else {
//     res.render("errors/404");
//   }
// });

router.post("/update", async (req, res) => {
  const { data, val, uname } = req.body;

  try {
    let user = await User.findOne({ userName: uname });
    if (user) {
      if (val == 0) user.fullName = data;
      if (val == 1) user.email = data;
      if (val == 2) user.about_me = data;
      await user.save();
      res.json({ status: 2000, msg: "Information Updated.." });
    } else {
      res.json({ status: 4000, msg: "user not found" });
    }
  } catch (err) {
    res.json({ status: 5000, msg: err });
  }
});

module.exports = router;

const router = require("express").Router();
const mongoose = require("mongoose");
const Fonts = require("../../../models/fonts");
const Story = require("../../../models/stories");
const User = require("../../../models/users");

router.get("*", (req, res, next) => {
  if (req.user) {
    next();
  } else {
    res.redirect("/login");
  }
});

router.get("/:userName", (req, res) => {
  if (req.user.userName.toLowerCase() === req.params.userName.toLowerCase()) {
    res.render("profile/index", req.user);
  } else {
    res.redirect("/access-denied");
  }
});

router.get("/:userName/study-room", async (req, res) => {
  if (req.user.userName.toLowerCase() === req.params.userName.toLowerCase()) {
    let user = req.user;
    let fontsArr = [];
    let fonts = await Fonts.find({});
    let objVal = {
      fonts,
      user
    };

    res.render("profile/study-room", objVal);
  } else {
    res.redirect("/access-denied");
  }
});

router.get("/:userName/stories", async (req, res) => {
  if (req.user.userName.toLowerCase() === req.params.userName.toLowerCase()) {
    let user = req.user;
    let fontsArr = [];
    let stories = await Story.find({ publisher_id: user.userName });
    let objVal = {
      stories,
      user
    };

    res.render("profile/stories", objVal);
  } else {
    res.redirect("/access-denied");
  }
});

router.get("/:userName/editor-room/:title", async (req, res) => {
  if (req.user.userName.toLowerCase() === req.params.userName.toLowerCase()) {
    let user = req.user;
    let fontsArr = [];
    let fonts = await Fonts.find({});

    let storyData = await Story.findOne({
      publisher_id: req.user.userName,
      link: req.params.title
    });
    if (!storyData) {
      res.render("errors/404-story");
    } else {
      let objVal = {
        fonts,
        user,
        storyData
      };
      res.render("profile/edit-room", objVal);
    }
  } else {
    res.render("errors/401");
  }
});

//editing saving url & rules
router.post("/:userName/editor-room/:title/save", async (req, res) => {
  if (req.body.txt.length < 100) {
    res.json({
      status: 400,
      msg: "Story Too Short. Story must contain al least 100 characters"
    });
  }
  if (req.user.userName === req.params.userName) {
    let msg = "Story Updated...";
    let cng = {
      willCng: false,
      link: "#"
    };
    try {
      let storyData = await Story.findOne({
        publisher_id: req.user.userName,
        link: req.params.title
      });

      if (storyData) {
        storyData.s_body.html = req.body.html;
        storyData.s_body.txt = req.body.txt;
        storyData.s_body.content = req.body.content;

        if (req.body.isPublish === "true") {
          storyData.published = true;
          storyData.p_date = Date.now();
          msg = "Story Updated & Published";
        }
        if (req.body.isTitleChanged === "true") {
          let title = await titleMatchingTest(req);
          if (!title) {
            res.json({ status: 400, msg: "Story with same title exist...." });
          } else {
            storyData.title = req.body.title;
            storyData.t_trimed = req.body.title.trim();
            storyData.link = title.toLowerCase();
            cng.willCng = true;
            cng.link = title;
            let user = await User.findOne({ userName: req.user.userName });
            if (user) {
              storyData.shadow_clone.name = user.fullName;
              storyData.shadow_clone.email = user.email;
              storyData.shadow_clone.img = user.profile_pic;
              storyData.shadow_clone.profile = user.userName;
              await storyData.save();
            } else {
              storyData.shadow_clone = {
                fullName: "Anonymous",
                profile_pic: "default.jpg"
              };
            }
            storyData.save();
          }
        } else {
          storyData.save();
        }

        res.json({ status: 200, msg: msg, cng: cng });
      } else {
        res.json({ status: 400, msg: "Network Error Occured..." });
      }
    } catch (e) {
      console.log(e);
      res.json({ status: 500, err: Error(e) });
    }
  } else {
    res.render("errors/401");
  }
});

let titleMatchingTest = async req => {
  let newTitle = replaceAll(req.body.title, " ", "-");
  let data = await Story.findOne({
    publisher_id: req.user.userName,
    t_trimed: req.body.title.trim()
  });

  if (data) {
    return false;
  } else {
    data = null;

    data = await Story.findOne({ link: newTitle });
    if (data) {
      newTitle += "-" + data.t_matched;
      data.t_matched += 1;
      await data.save();
      return newTitle;
    }
  }
  return newTitle;
};

let replaceAll = (str, find, replace) => {
  return str.replace(new RegExp(find, "g"), replace);
};

module.exports = router;

const router = require("express").Router();
const mongoose = require("mongoose");
const Story = require("../../../models/stories");
const User = require("../../../models/users");

router.post("/save", async (req, res) => {
  if (req.body.txt.length < 100) {
    res.json({
      status: 7000,
      err: "Story Too Short. Story must contain al least 100 characters"
    });
  }

  let isExist = await Story.findOne({
    publisher_id: req.user.userName,
    t_trimed: req.body.title.trim()
  });
  if (isExist) {
    res.json({ status: 7000, err: "Story Exist" });
  } else {
    let saveData = await saveStory(req);
    if (saveData) {
      res.json({ l_link: saveData.link, _id: saveData.s_ID, status: 200 });
    } else {
      res.json({ status: 400, err: "problems" });
    }
  }
});

router.post("/publish", async (req, res) => {
  let isExist = await Story.findOne({
    publisher_id: req.user.userName,
    t_trimed: req.body.title.trim()
  });
  if (isExist) {
    res.json({ status: 7000, err: "Story With Same Title Already Exist" });
  } else {
    try {
      let data = await saveStory(req);
      if (data) {
        let id = data.s_ID;
        let item = await Story.findOne({ s_ID: id });
        if (item) {
          item.published = true;
          item.p_date = new Date();
          item.save();
        }
        res.json({ l_link: data.link, _id: data.s_ID, status: 200 });
      } else {
        res.json({ status: 400, err: e });
      }
    } catch (e) {
      res.json({ status: 400, err: e });
    }
  }
});

//saving story
let saveStory = async req => {
  let l_title = replaceAll(req.body.title, " ", "-");

  try {
    let isMatched = await Story.findOne({ link: l_title });

    if (isMatched) {
      let isUser = await Story.findOne({
        link: l_title,
        publisher_id: req.user.userName
      });
      if (isUser) {
        res.json({ status: 400, err: "story exist" });
      }

      l_title += "-" + isMatched.l_matched;
      isMatched.l_matched += 1;
      await isMatched.save();
    } else {
      let id = Date.now();
      let d = new Date();

      let user = await User.findOne({ userName: req.user.userName });

      //new story object
      let newStory = new Story({
        s_ID: id,
        title: req.body.title,
        t_trimed: req.body.title.trim(),
        genre: req.body.genre,
        s_body: {
          content: req.body.content,
          html: req.body.html,
          txt: req.body.txt
        },
        publisher_id: req.body.user,
        link: l_title.toLowerCase(),
        shadow_clone: {
          name: user.fullName,
          email: user.email,
          img: user.profile_pic,
          profile: user.userName
        }
      });

      let saveIt = await newStory.save();
      return newStory;
    }
  } catch (e) {
    console.log(e);
    return false;
  }

  return false;
};

let replaceAll = (str, find, replace) => {
  return str.replace(new RegExp(find, "g"), replace);
};

module.exports = router;

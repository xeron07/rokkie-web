const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const fs = require("fs");
const key = require("../../config/keys");
const nodemailer = require("nodemailer");
const sgTransport = require("nodemailer-sendgrid-transport");
const mongoose = require("mongoose");
const User = require("../../models/users");

router.get("/", async (req, res) => {
  res.render("signup/index");
});

router.post("/user", async (req, res) => {
  const { uname, name, email, password, password_c, gender } = req.body;
  let errors = [];

  //empty fields
  if (
    uname === "" ||
    name === "" ||
    email === "" ||
    password === "" ||
    password_c === ""
  ) {
    errors.push({ msg: "Please fill up all the fields" });
  }

  //check password
  if (password !== password_c) {
    errors.push({ msg: "Password doesn't matched" });
  } else {
    if (password.length < 6)
      errors.push({
        msg: "Password length should be at least 6 characters long"
      });
  }

  if (errors.length > 0) {
    res.render("signup/index", {
      errors,
      uname,
      name,
      email
    });
  } else {
    try {
      let user = await User.findOne({ userName: uname });
      if (user) {
        errors.push({ msg: "User with same user name exist" });
        res.render("signup/index", {
          errors,
          uname,
          name,
          email
        });
      } else {
        try {
          let userEmail = await User.findOne({ email: email });
          if (userEmail) {
            errors.push({ msg: "Email already used.." });
            res.render("signup/index", {
              errors,
              uname,
              name,
              email
            });
          } else {
            let salt = await bcrypt.genSalt(10);
            let hashPassword = await bcrypt.hash(password, salt);
            let imgName = "";
            if (gender === "male") {
              imgName = "default_male.jpg";
            } else if (gender === "female") {
              imgName = "default_female.jpg";
            } else {
              imgName = "default.png";
            }

            let newUser = new User({
              userName: uname,
              fullName: name,
              password: hashPassword,
              email: email,
              gender: gender,
              profile_pic: imgName
            });

            try {
              user = await newUser.save();
              sendMail(email);
              req.flash(
                "success_msg",
                "Registration Successful please login now."
              );
              res.redirect("/login");
            } catch (e) {
              console.error("error= " + e);
            }
          }
        } catch (e) {
          console.error("error= " + e);
        }
      }
    } catch (e) {
      console.error("error= " + e);
    }
  }
});
let mOptions = {
  host: "smtp.ethereal.email",
  port: 587,
  sendMail: true,
  auth: {
    user: key.mail_key.api_u,
    pass: key.mail_key.api_pass
  }
};
let transporter = nodemailer.createTransport(mOptions);

let sendMail = async email => {
  let htmlData = "";
  fs.readFile("./mail/abc.html", (err, data) => {
    if (err) console.log(err);
    else {
      htmlData = data.toString("utf8");
      let htmlData2 =
        '  <a class="center" type="button">Confirm</a></div>  <br /><div style="margin-left:20%"><p>----------------</p><h5>Thank you.</h5><h5>Rokki-inc</h5></div>';
      let mailOptions = {
        from: "rokki-inc@yandex.com",
        to: email,
        subject: "Verification mail from rokki-inc",
        html: htmlData + htmlData2
        // text: "abc"
      };
      console.log(htmlData);
      transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log("Email sent: " + info.response);
        }
      });
    }
  });
};

module.exports = router;

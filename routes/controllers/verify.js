const router = require("express").Router();
const mongoose = require("mongoose");
const User = require("../../models/users");
router.get("/:hash", async (req, res) => {
  let hashCode = req.params.hash;
  let user = await User.findOne({ hash: hashCode });
  if (user) {
    user.active = true;
    await user.save();
    req.flash("success_msg", "Registration & Verification succesfully done.");
    res.redirect("/login");
  } else {
    res.render("errors/404");
  }
});

module.exports = router;

const router = require("express").Router();
const mongoose = require("mongoose");
const Story = require("../../models/stories");
const User = require("../../models/users");

router.get("/:st_link", async (req, res) => {
  let story = await Story.findOne({
    link: req.params.st_link,
    published: true
  });
  if (story) {
    let writer = await User.findOne({ userName: story.publisher_id });

    let obj = {
      story,
      writer
    };
    res.render("read/index", obj);
  }
  res.render("errors/404-story");
});

module.exports = router;

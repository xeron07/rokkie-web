const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const session = require("express-session");
const flash = require("connect-flash");
const passport = require("passport");
const download = require("downloadjs");
const firstPage = require("./routes/controllers/default");
require("./services/localPassport")(passport);
const login = require("./routes/controllers/login");
const signup = require("./routes/controllers/signup");
const home = require("./routes/controllers/home");
const profile = require("./routes/controllers/profile/profile");
const logout = require("./routes/controllers/logout");
//const addFont = require("./routes/controllers/admin/addfonts");
const story_item = require("./routes/controllers/profile/story");
const read = require("./routes/controllers/read");
const demoRead = require("./routes/controllers/profile/demo");
const userController = require("./routes/controllers/profile/user");

//story component link
const sc = require("./routes/controllers/profile/storyComponent");
const keys = require("./config/keys");

const app = express();

//CONFIGURATIONS
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.locals.download = download;
app.set("view engine", "ejs");
app.use("/ext", express.static("ext"));
app.use("/media", express.static("ftp-server/media"));

//session configuration
app.use(
  session({
    secret: keys.s_key.gingerbread,
    resave: true,
    saveUninitialized: true
  })
);
//passport configuration
app.use(passport.initialize());
app.use(passport.session());

//flash configuration
app.use(flash());
app.use((req, res, next) => {
  res.locals.success_msg = req.flash("success_msg");
  res.locals.error_msg = req.flash("error_msg");
  res.locals.error = req.flash("error");
  next();
});

mongoose.connect(keys.mongoDb.dbString, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

//collections
require("./models/fonts");

//MIDDLEWARES
app.use("/", firstPage);
app.use("/story", home);
app.use("/login", login);
app.use("/signup", signup);
app.use("/profile", profile);
app.use("/logout", logout);
//app.use("/addFont", addFont);
app.use("/item", story_item);
app.use("/sc", sc);
app.use("/read", read);
app.use("/demo", demoRead);
app.use("/uc", userController);
app.get("/access-denied", (req, res) => {
  res.status(401).render("errors/401");
});
app.use("/404-story", (req, res) => {
  res.status(404).render("404-story");
});

const PORT = process.env.PORT || 6776;

//404 error route
app.use(function(req, res, next) {
  res.status(404).render("errors/404", { title: "Sorry, page not found" });
});
app.listen(PORT, () => {
  console.log("Server Running ................" + PORT);
});

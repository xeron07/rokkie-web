const mongoose = require("mongoose");
const { Schema } = mongoose;

const storySchema = new Schema({
  s_ID: String,
  title: String,
  genre: String,
  fonts_uses: [
    {
      name: String,
      link: String
    }
  ],
  liked_by: Array,
  t_trimed: String,
  s_body: {
    content: JSON,
    html: String,
    txt: String
  },
  shadow_clone: {
    name: String,
    email: String,
    img: String,
    profile: String
  },
  t_matched: { type: Number, default: 1 },
  download_able: { type: Boolean, default: true },
  lang: { type: String, default: "en" },
  published: { type: Boolean, default: false },
  p_date: { type: Date, default: Date.now() },
  publisher_id: String,
  s_img: String,
  edited: { type: Number, default: 0 },
  likes: { type: Number, default: 0 },
  link: String,
  comments: [
    {
      body: String,
      date: { type: Date, default: Date.now() },
      reply: [{ body: String, date: Date }]
    }
  ]
});

module.exports = mongoose.model("stories", storySchema);

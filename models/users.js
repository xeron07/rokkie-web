const mongoose = require("mongoose");
const { Schema } = mongoose;

const userSchema = new Schema({
  userId: String,
  userName: String,
  fullName: String,
  email: String,
  password: String,
  about_me: String,
  rating: Number,
  profile_pic: { type: String, default: "default.png" },
  gender: String,
  h_key: String,
  active: { type: Boolean, default: false },
  bane: { type: Boolean, default: false },
  b_duration: { type: Number, default: 36000 },
  g_id: Number
});

module.exports = mongoose.model("users", userSchema);

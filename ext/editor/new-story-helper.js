let selectStyle = a => {
  $(".select-dropdown").css({
    "border-bottom-color": "#27ae60",
    "border-bottom-size": "2px",
    color: "#f1f2f6"
  });
  $("#sTitle").css("display", "none");
};

let saveStory = x => {
  if ($("#story-title").val() !== "" && $("#selectItems").val() != null) {
    $("#test").html(val.toString());
    $("#buttons").fadeOut(1000, async () => {
      $("#loader").fadeIn();

      saveData(x)
        .then(
          res => {
            if (res.operation === "Success") {
              showAgain("success", "Story Saved....");
              setTimeout(() => {
                window.location.href =
                  "/profile/" + userName + "/editor-room/" + res.link;
              }, 5000);
            }
          },
          err => {
            showAgain("error", err);
          }
        )
        .catch();
    });
  } else {
    console.log("no-title");
    if ($("#story-title").val() === "") {
      $("#story-title").addClass("invalid");
      $("#story-title").prop("aria-invalid", "true");
    }
    if ($("#selectItems").val() == null) {
      console.log("in");
      $(".select-dropdown").css({
        "border-bottom-color": "#ff3838",
        "border-bottom-size": "5px"
      });
      $("#sTitle").css("display", "inline");
    }
    Swal.fire("Oops!!", "Please fill up all the fields", "error");
  }
};
let showAgain = (res, msg) => {
  //show messeage
  $("#loader").fadeOut(1000, () => {
    if (res === "success") {
      $("#s_msg").text(msg);
      $("#success-message").fadeIn(1000);
    } else {
      $("#e_msg").text(msg);
      $("#error-message").fadeIn(1000);
    }

    //Show buttons
    setTimeout(() => {
      if (res === "success") {
        $("#success-message").fadeOut(1000, () => {
          $("#buttons").fadeIn(1000);
        });
      } else {
        $("#error-message").fadeOut(1000, () => {
          $("#buttons").fadeIn(1000);
        });
      }
    }, 5000);
  });
};

let saveData = x => {
  return new Promise((resolve, reject) => {
    let val = "none";
    let link = "";
    console.log(val);
    if (x === "save") link = "/item/save";
    else if (x === "publish") link = "/item/publish";
    $.ajax({
      method: "POST",
      url: link,
      data: {
        html: quill.root.innerHTML,
        content: JSON.stringify(quill.getContents()),
        txt: quill.getText(),
        user: userName,
        title: $("#story-title").val(),
        genre: $("#selectItems").val()
      },
      success: res => {
        console.log("done");
        if (res.status === 200) {
          Swal.fire({
            position: "top-end",
            type: "success",
            title: "Your work has been saved",
            showConfirmButton: false,
            timer: 2000
          });

          let obj = {
            operation: "Success",
            link: res.l_link
          };
          resolve(obj);
        } else if (res.status === 7000) {
          Swal.fire({
            position: "top-end",
            type: "warning",
            title: "Your work not saved",
            showConfirmButton: false,
            timer: 2000
          });
          reject(Error(res.err));
        } else {
          reject(Error("Failed"));
        }
      },
      error: err => {
        console.error(err);
        reject(
          Error("Network Problem. Check your internet connection........")
        );
      }
    });
  });
};

let blockConsole = () => {
  console.clear();
  setTimeout(blockConsole, 1000);
};

// let storyPublish=()=>{
//     if($("#story-title").val()!=="" && $("#selectItems").val()!=null)
//     {
//       $("#test").html(val.toString());
//           $("#buttons").fadeOut(1000,()=>{
//               $("#loader").fadeIn();
//               let result=saveData();

//     });
//     }
//     else{
//         console.log("no-title");
//     }
//   }

//   let publish=()=>{
//      saveData().then((res)=>{
//        alert(res);
//      },(err)=>{
//        alert(err);
//      }).catch((err)=>{
//        console.log(err);
//      });
//   }
